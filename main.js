//Load libraries 
var express = require("express");
var path = require("path");

//instance for express
var app = express();

//set the directory name 
app.use(express.static(__dirname+"/public"));
app.use("/libs",express.static(__dirname+"/bower_components"));
app.use("/stylesheet",express.static(__dirname+"/stylesheet"));

//set up the port 
app.set ("port",parseInt(process.argv[2])||3000);

//connect the server 
app.listen(app.get("port"),function(){
console.log("Application is running on Date : %d at Port : %d"
            ,new Date(),app.get("port"));
});

