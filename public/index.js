//start angular  --  IIFE immidetaly invoked function expression 

(function(){

//Create instance of angular app 
var CartApp = angular.module("CartApp",[]);


//Define controller function()
var CartCtrl = function(){
    var cartCtrl = this;

    cartCtrl.item = ""; // same as this.item
    cartCtrl.quantity = 0; // same as this.quantity
    cartCtrl.filterText ="";
    cartCtrl.basket = [];

    //remove item from basket based on index property of ng-repeat
     cartCtrl.removeFromBasket = function(item) {
            var idx = cartCtrl.basket.findIndex(function(elem) {
                return (item == elem.item);
            })
            if (idx >= 0)
                cartCtrl.basket.splice(idx, 1);
        }

    cartCtrl.addToBasket = function(){
        //check item in cartCtrl.item to basket[] if != then add.
       var idx = cartCtrl.basket.findIndex(function(elem){
           return (elem.item == cartCtrl.item)
       });

       if(-1==idx)
        //push item to cart
       cartCtrl.basket.push({
           item:cartCtrl.item,
           quantity:cartCtrl.quantity
       });
       else 
        cartCtrl.basket[idx].quantity += cartCtrl.quantity;
       
       //reset cart
       cartCtrl.item="";
       cartCtrl.quantity=0;
    };
}

//Create a controller
CartApp.controller("CartCtrl",[CartCtrl]); 

})();

/* ---described a below in book its same as above 
    angular.module("CartApp",[])
        .controller("CartCtrl",[fucntion(){
            var myCart = this;
            myCart.item = ""; 
            myCart.quantity = 0;
        }]) */
