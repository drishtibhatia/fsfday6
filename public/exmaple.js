//create a function to create object 
var creatPerson = function(name,email){
    var person ={
        name : name,
        email: email,
        greet : function(){
            console.log("hello %s",this.name);
        }
    };
    return (person);
};

var Person = function(name,email){
        this.name = name;
        this.email =  email;
        //this.basket = [];
        /* adding new property drectly inside object. Value may be diffrent 
        this.myemail = function(){
            console.log("My email is %s", this.email);
        } */ 
}
// adding new property that would be sahred by all objected created using constructor 
//can be craeted using console. 
Person.prototype.basket = [];
Person.prototype.myemail = function(){
    console.log("My email is %s", this.email);
}

var fred = creatPerson("fred","fred@gami.com");
/* calling function createPerson to create boject fred 
var braney = creatPerson("barney","b@gami.com"); */


//function created using 'new' is called function consturctor
var drishti = new Person("drishti","drishti@d.com");
var jay = new Person("jay","jay@gmail.com");

/*Person("jay","jay@g.com"); 
it is set in environment varibale */

/*fred obeject is created using function 
var fred = {
    name:"fred",
    email:"fred@bedroc.com"
}; */



